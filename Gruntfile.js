module.exports = function(grunt){
	//time-grunt - para las estadisticas de los tiempos de ejecucion
	require('time-grunt')(grunt);
	//jit-grunt - para la carga de tareas automaticas
	require('jit-grunt')(grunt, {
		useminPrepare: 'grunt-usemin'// minificar los archivos
	});

	grunt.initConfig({
		//Sass - para actualizar todos los archivos css
		sass:{
			dist:{
				files: [{
					expand: true,
					cwd: 'css',
					src: ['*.scss'],
					dest: 'css', 
					ext: '.css'
				}]
			}
		},
		//Watch - para escuchar y actualizar
		watch:{
			files: ['css/*.scss'],
			tasks: ['css']
		},
		//BrowserSync - sincroniza el navegador automaticamente con todos los css, js y html
		browserSync:{
			dev:{
				bsFiles:{ // browser files
					src: [
					'css/*.css',
					'*.html',
					'js/*.js'
					]
				},
			},
			options: {
				watchTask : true, 
				server: {
					baseDir: './' //Directorio base para nuestro servidor
				}
			}
		},
		//ImagenMin - comprime las imagenes de ./images -> dist/images/
		imagemin:{
			dynamic:{
				files:[{
					expand: true, 
					cwd: './', 
					src: ['images/*.{jpg, jpeg}'], //{png, gif, jpg, jpeg}'], 
					dest:'dist'
				}]
			}
		},
		//copy - librerias
		copy:{
			html:{
				files:[{
					expand: true,
					dot: true,
					cwd: './', 
					src: ['*.html'], 
					dest: 'dist'
				}]
			},
			fonts: {
				files: [{
					// para el font-awesome
					expand: true,
					dot: true, 
					cwd: 'node_modules/open-iconic/font',
					src: ['fonts/*.*'],
					dest: 'dist'
				}]
			},
			ccs: {
				files: [{
					// para los css que no se copian
					expand: true,
					dot: true, 
					cwd: './',
					src: ['css/*.css'],
					dest: 'dist'
				}]
			}, 
			// LIBRERIAS
			jquery:{
					// para el jquery
					expand: true,
					dot: true, 
					cwd: 'node_modules/jquery/dist/',
					src: ['*.*'],
					dest: 'dist/node_modules/jquery/dist'
			},
			boostrapjs:{
					// para el boostrap js
					expand: true,
					dot: true, 
					cwd: 'node_modules/bootstrap/dist/',
					src: ['js/*.*'],
					dest: 'dist/node_modules/bootstrap/dist'
			},
			boostrapcss:{
					// para el boostrap css
					expand: true,
					dot: true, 
					cwd: 'node_modules/bootstrap/dist/',
					src: ['css/*.*'],
					dest: 'dist/node_modules/bootstrap/dist'
			},
			openiconic:{
					// para open-iconic
					expand: true,
					dot: true, 
					cwd: 'node_modules/open-iconic/font/css/',
					src: ['*.*'],
					dest: 'dist/node_modules/open-iconic/font/css'
			}
		},
		//clean - eliminar y limpiar carpetas
		clean: {
			build: {
				src: ['dist/']
			}
		},
		//cssmin -
		cssmin: {
			dist: {}
		},
		//uglify -
		uglify: {
			dist: {}
		},
		//filerev - genera un cogido cachable
		filerev: {
			option:{
				encoding: 'utf8',
				algorithm: 'md5',
				length: 20
			},
			release:{
				//filerev:release hashes(md5) all assets (images, js and css)
				// in dist directory
				files:[{
					src: [
					'dist/js/*.js',
					'dist/css/*.css',
					]
				}]
			}
		},
		//concat - concatenacion de archivos
		concat: {
			options:{
				separator: ';'
			}, 
			dist: {}
		},
		//useminPrepare - a todos los archivos de html, se les aplica los procesos de cssmin y uglify; luego se guarda en dist
		useminPrepare:{
			foo: {
				dest: 'dist',
				src: ['about.html','contacto.html', 'index.html', 'precios.html']
			},
			options: {
				flow: {
					steps: {
						css: ['cssmin'],
						js: ['uglify']
					},
					post:{
						css: [{
							name: 'cssmin',
							createConfig: function(context, block){
								var generated = context.options.generated;
							}
						}]
					}
				}
			}
		},
		//usemin - buscar estos archivos html y tomar los assets en el directorios especificado
		usemin: {
			html: ['dist/about.html', 'dist/contacto.html', 'dist/index.html', 'dist/precios.html'],
			options: {
				assetsDir: ['dist', 'dist/css', 'dist/js']
			}
		}
	});
	//cargando pluggins	
	// grunt.loadNpmTasks('grunt-contrib-watch');
	// grunt.loadNpmTasks('grunt-contrib-sass');
	// grunt.loadNpmTasks('grunt-browser-sync');
	// grunt.loadNpmTasks('grunt-contrib-imagemin');
	//registrando tarea
	grunt.registerTask('css', ['sass']);
	grunt.registerTask('default', ['browserSync', 'watch']);
	grunt.registerTask('img:compress', ['imagemin']);
	grunt.registerTask('build', ['clean', 'copy', 'imagemin', 'useminPrepare', 'concat', 'cssmin', 'uglify', 'filerev', 'usemin']);
};