'use strict'
//Variables
const gulp = require( 'gulp' ); //Cargamos nuestro modulo gulp
const sass = require('gulp-sass')(require('sass')); // nueva version
const browserSync = require('browser-sync');
//const browserSync = require('browser-sync')(require('sass')); // nueva version
//const browserSync = require('browser-sync').create();

const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usermin');
const rev  = require('gulp-rev');
const cleanCcs = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

//tareas
gulp.task('sass', function() { 
	gulp.src('./css/*.scss')
	.pipe(sass().on('error',sass.logError))
	.pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function(){
	gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function(){
	var files = ['./*.html', './css/*.css', './images/*.{png, jpg, jpeg, gif}', './js/*.js'];
	browserSync.init(files, {
		server: {
			baseDir: './'
		}
	});
});

/*
gulp.task('default', ['browser-sync'], function(){
	//gulp.start('browser-sync');
	gulp.start('sass:watch');
});
*/

gulp.task('default', function(){
	gulp.start('sass');
	//gulp.start('browser-sync');
	//gulp.start('sass:watch');
});


//TAREAS DE DISTRIBUCION
gulp.task('clean', function(){
	return del(['dist']);//borra la carpeta dist
});

gulp.task('copyfonts', function(){
	return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf, woff, eof, svg, eot, otf}*')
	.pipe(gulp.dest('./dist/fonts'));
});

gulp.task('imagemin', function(){
	return gulp.src('./images/*.{png, jpg, jpeg, gif}')
	.pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
	.pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', function(){
	return gulp.src('./*.html')
	.pipe(flatmap(function(stream, file){
		return stream
			.pipe(usemin({
				css: [rev()],
				html: [function(){
					return htmlmin({collapseWhitespace: true})
				}],
				js: [uglify(), rev()],
				inlinejs: [uglify()],
				inlinecss: [cleanCcs(), 'concat'],
			}));
	}))
	.pipe(gulp.dest('dist/'));
});

gulp.task('build', ['clean'], function(){
	gulp.star('copyfonts','imagemin', 'usemin');
});